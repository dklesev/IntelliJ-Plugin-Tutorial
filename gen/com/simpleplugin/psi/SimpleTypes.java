// This is a generated file. Not intended for manual editing.
package com.simpleplugin.psi;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import com.simpleplugin.SimpleElementType;
import com.simpleplugin.SimpleTokenType;
import com.simpleplugin.psi.impl.*;

public interface SimpleTypes {

  IElementType DEF = new SimpleElementType("DEF");
  IElementType PROPERTY = new SimpleElementType("PROPERTY");
  IElementType SCHEMA = new SimpleElementType("SCHEMA");

  IElementType COMMENT = new SimpleTokenType("comment");
  IElementType DEF_START = new SimpleTokenType("#");
  IElementType EQ = new SimpleTokenType("=");
  IElementType LP = new SimpleTokenType("(");
  IElementType NAMESPACES = new SimpleTokenType("namespaces");
  IElementType NEWLINE = new SimpleTokenType("newline");
  IElementType NUMBER = new SimpleTokenType("number");
  IElementType REF = new SimpleTokenType("$ref");
  IElementType RP = new SimpleTokenType(")");
  IElementType SEMI = new SimpleTokenType(";");
  IElementType SEPARATOR = new SimpleTokenType("/");
  IElementType SPACE = new SimpleTokenType("space");
  IElementType STRING = new SimpleTokenType("string");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
       if (type == DEF) {
        return new SimpleDefImpl(node);
      }
      else if (type == PROPERTY) {
        return new SimplePropertyImpl(node);
      }
      else if (type == SCHEMA) {
        return new SimpleSchemaImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}
