// This is a generated file. Not intended for manual editing.
package com.simpleplugin.parser;

import com.intellij.lang.PsiBuilder;
import com.intellij.lang.PsiBuilder.Marker;
import static com.simpleplugin.psi.SimpleTypes.*;
import static com.intellij.lang.parser.GeneratedParserUtilBase.*;
import com.intellij.psi.tree.IElementType;
import com.intellij.lang.ASTNode;
import com.intellij.psi.tree.TokenSet;
import com.intellij.lang.PsiParser;
import com.intellij.lang.LightPsiParser;

@SuppressWarnings({"SimplifiableIfStatement", "UnusedAssignment"})
public class SimpleParser implements PsiParser, LightPsiParser {

  public ASTNode parse(IElementType t, PsiBuilder b) {
    parseLight(t, b);
    return b.getTreeBuilt();
  }

  public void parseLight(IElementType t, PsiBuilder b) {
    boolean r;
    b = adapt_builder_(t, b, this, null);
    Marker m = enter_section_(b, 0, _COLLAPSE_, null);
    if (t == DEF) {
      r = def(b, 0);
    }
    else if (t == PROPERTY) {
      r = property(b, 0);
    }
    else if (t == SCHEMA) {
      r = schema(b, 0);
    }
    else {
      r = parse_root_(t, b, 0);
    }
    exit_section_(b, 0, m, t, r, true, TRUE_CONDITION);
  }

  protected boolean parse_root_(IElementType t, PsiBuilder b, int l) {
    return root(b, l + 1);
  }

  /* ********************************************************** */
  // ( def_start separator namespaces separator? )+ schema?
  public static boolean def(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "def")) return false;
    if (!nextTokenIs(b, DEF_START)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = def_0(b, l + 1);
    r = r && def_1(b, l + 1);
    exit_section_(b, m, DEF, r);
    return r;
  }

  // ( def_start separator namespaces separator? )+
  private static boolean def_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "def_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = def_0_0(b, l + 1);
    int c = current_position_(b);
    while (r) {
      if (!def_0_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "def_0", c)) break;
      c = current_position_(b);
    }
    exit_section_(b, m, null, r);
    return r;
  }

  // def_start separator namespaces separator?
  private static boolean def_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "def_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, DEF_START, SEPARATOR, NAMESPACES);
    r = r && def_0_0_3(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // separator?
  private static boolean def_0_0_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "def_0_0_3")) return false;
    consumeToken(b, SEPARATOR);
    return true;
  }

  // schema?
  private static boolean def_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "def_1")) return false;
    schema(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // ref space* ':' space* def
  public static boolean property(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "property")) return false;
    if (!nextTokenIs(b, REF)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, PROPERTY, null);
    r = consumeToken(b, REF);
    r = r && property_1(b, l + 1);
    p = r; // pin = 2
    r = r && report_error_(b, consumeToken(b, ":"));
    r = p && report_error_(b, property_3(b, l + 1)) && r;
    r = p && def(b, l + 1) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // space*
  private static boolean property_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "property_1")) return false;
    int c = current_position_(b);
    while (true) {
      if (!consumeToken(b, SPACE)) break;
      if (!empty_element_parsed_guard_(b, "property_1", c)) break;
      c = current_position_(b);
    }
    return true;
  }

  // space*
  private static boolean property_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "property_3")) return false;
    int c = current_position_(b);
    while (true) {
      if (!consumeToken(b, SPACE)) break;
      if (!empty_element_parsed_guard_(b, "property_3", c)) break;
      c = current_position_(b);
    }
    return true;
  }

  /* ********************************************************** */
  // root_item *
  static boolean root(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "root")) return false;
    int c = current_position_(b);
    while (true) {
      if (!root_item(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "root", c)) break;
      c = current_position_(b);
    }
    return true;
  }

  /* ********************************************************** */
  // !<<eof>> property ( newline | space )*
  static boolean root_item(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "root_item")) return false;
    if (!nextTokenIs(b, REF)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = root_item_0(b, l + 1);
    r = r && property(b, l + 1);
    r = r && root_item_2(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // !<<eof>>
  private static boolean root_item_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "root_item_0")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NOT_);
    r = !eof(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // ( newline | space )*
  private static boolean root_item_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "root_item_2")) return false;
    int c = current_position_(b);
    while (true) {
      if (!root_item_2_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "root_item_2", c)) break;
      c = current_position_(b);
    }
    return true;
  }

  // newline | space
  private static boolean root_item_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "root_item_2_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, NEWLINE);
    if (!r) r = consumeToken(b, SPACE);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // namespaces
  public static boolean schema(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "schema")) return false;
    if (!nextTokenIs(b, NAMESPACES)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, NAMESPACES);
    exit_section_(b, m, SCHEMA, r);
    return r;
  }

}
