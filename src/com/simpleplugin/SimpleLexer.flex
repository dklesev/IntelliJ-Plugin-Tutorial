package ;
import com.intellij.lexer.*;
import com.intellij.psi.tree.IElementType;
import static com.simpleplugin.psi.SimpleTypes.*;

%%

%{
  public _SimpleLexer() {
    this((java.io.Reader)null);
  }
%}

%public
%class com.simpleplugin.SimpleLexer
%implements FlexLexer
%function advance
%type IElementType
%unicode

EOL="\r"|"\n"|"\r\n"
LINE_WS=[\ \t\f]
WHITE_SPACE=({LINE_WS}|{EOL})+

NEWLINE=\n*
SPACE=[ \t\n\x0B\f\r]+
COMMENT="//".*
NUMBER=[0-9]+(\.[0-9]*)?
STRING=('([^'\\]|\\.)*'|\"([^\"\\]|\\.)*\")
NAMESPACES=[:letter:][a-zA-Z_0-9]*

%%
<YYINITIAL> {
  {WHITE_SPACE}      { return com.intellij.psi.TokenType.WHITE_SPACE; }

  ";"                { return SEMI; }
  "="                { return EQ; }
  "("                { return LP; }
  ")"                { return RP; }
  "$ref"             { return REF; }
  "#"                { return DEF_START; }
  "/"                { return SEPARATOR; }

  {NEWLINE}          { return NEWLINE; }
  {SPACE}            { return SPACE; }
  {COMMENT}          { return COMMENT; }
  {NUMBER}           { return NUMBER; }
  {STRING}           { return STRING; }
  {NAMESPACES}       { return NAMESPACES; }

  [^] { return com.intellij.psi.TokenType.BAD_CHARACTER; }
}
